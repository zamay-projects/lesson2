import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const fighterInfo = createElement({
      tagName: 'div',
      className: 'fighter__info'
    });

    fighterElement.style.cssText = `
    justify-content: space-between;
    height: 100%;
    flex-basis: 200px;`;

    fighterInfo.style.cssText = `
      color: antiquewhite;
      font-size: larger;
      padding: 0 20px;
      background: #ba0303;
      margin: 0 auto;
      color: #f8f9f4;
      font-size: 22px;
      cursor: default;
      font-weight: bold;
      box-shadow: 6px 6px 7px 0px rgb(0 0 0 / 75%);`;

    fighterInfo.innerHTML = `
      <p>${fighter?.name}</p>
      <p>Attack: ${fighter?.attack}</p>
      <p>Defense: ${fighter?.defense}</p>
      <p>Health: ${fighter?.health}</p>`;

    fighterElement.append(fighterInfo)

    const fighterImg = fighter && createFighterImage(fighter)
    fighterElement.append(fighterImg)
  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
