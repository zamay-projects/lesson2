import { showModal } from './modal';

export function showWinnerModal(fighter) {
  console.log(fighter);
  const newGame = document.createElement('div')
  newGame.classList.add('fighters___fighter-modal');
  newGame.innerHTML = `<img class="fighter___fighter-image" src="${fighter.source}"
                                           title="Ken" alt="Ken">`;
  const modalObj = {
    title: `${fighter.name} wins!`,
    bodyElement: newGame
  };
  showModal(modalObj);
}
