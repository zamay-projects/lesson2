import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    let firstFighterBlock = false;
    let secondFighterBlock = false;
    let firstFighterHP = firstFighter.health;
    let secondFighterHP = secondFighter.health;
    let isOneCriticalHit = true;
    let isTwoCriticalHit = true;
    let isPlayerOneAttacking = true;
    let isPlayerTwoAttacking = true;

    let pressed = new Set();

    window.addEventListener('keydown', (event) => {
      pressed.add(event.code);
      let allKeysPressedFirstFighter = true;
      let allKeysPressedSecondFighter = true;

      switch (event.code) {
        case controls.PlayerOneBlock:
          firstFighterBlock = true;
          break;
        case controls.PlayerTwoBlock:
          secondFighterBlock = true;
          break;
        case controls.PlayerOneAttack:
          if (!isPlayerOneAttacking || firstFighterBlock) return;

          const damageFirstFighter = secondFighterBlock? 0 : getDamage(firstFighter, secondFighter);
          secondFighterHP -= damageFirstFighter;

          reduceHealthIndicator('right', secondFighterHP, secondFighter.health);
          printResultToConsole(secondFighter, secondFighterHP, damageFirstFighter);

          if (secondFighterHP <= 0) {
            resolve(firstFighter);
          }
          isPlayerOneAttacking = false;
          break;
        case controls.PlayerTwoAttack:
          if (!isPlayerTwoAttacking || secondFighterBlock) return;

          const damageSecondFighter = firstFighterBlock? 0 : getDamage(secondFighter, firstFighter);
          firstFighterHP -= damageSecondFighter;

          reduceHealthIndicator('left', firstFighterHP, firstFighter.health);
          printResultToConsole(firstFighter, firstFighterHP, damageSecondFighter);

          if (firstFighterHP <= 0) {
            resolve(secondFighter);
          }
          isPlayerTwoAttacking = false;
          break;
      }

      for (let code of controls.PlayerOneCriticalHitCombination ) {
        if (!pressed.has(code)) {
          allKeysPressedFirstFighter = false;
          break;
        }
      }

      if (allKeysPressedFirstFighter && isOneCriticalHit) {
        const damageFirstFighter = getCritPower(firstFighter);
        secondFighterHP -= damageFirstFighter;

        reduceHealthIndicator('right', secondFighterHP, secondFighter.health);
        printResultToConsole(secondFighter, secondFighterHP, damageFirstFighter);

        if (secondFighterHP <= 0) {
          resolve(firstFighter);
        }

        isOneCriticalHit = false;
        critBarProgress('left', (isFinished) => {
          if (isFinished) {
            isOneCriticalHit = true;
          }
        });
      }

      for (let code of controls.PlayerTwoCriticalHitCombination ) {
        if (!pressed.has(code)) {
          allKeysPressedSecondFighter = false;
          break;
        }
      }

      if (allKeysPressedSecondFighter && isTwoCriticalHit) {
        const damageSecondFighter = getCritPower(secondFighter);
        firstFighterHP -= damageSecondFighter;

        reduceHealthIndicator('left', firstFighterHP, firstFighter.health);
        printResultToConsole(firstFighter, firstFighterHP, damageSecondFighter);

        if (firstFighterHP <= 0) {
          resolve(secondFighter);
        }

        isTwoCriticalHit = false;
        critBarProgress('right', (isFinished) => {
          if (isFinished) {
            isTwoCriticalHit = true;
          }
        });
      }

    });

    document.addEventListener('keyup', function(event){
      switch (event.code) {
        case controls.PlayerOneBlock:
          firstFighterBlock = false;
          break;
        case controls.PlayerTwoBlock:
          secondFighterBlock = false;
          break;
        case controls.PlayerOneAttack:
          isPlayerOneAttacking = true;
          break;
        case controls.PlayerTwoAttack:
          isPlayerTwoAttacking = true;
          break;
      }
      pressed.delete(event.code);
    });

  });
}

export function getDamage(attacker, defender) {
  let damage = getHitPower(attacker) - getBlockPower(defender);
  return damage < 0 ? 0 : damage;
}

export function getHitPower(fighter) {
  let criticalHitChance = Math.random() + 1
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  let dodgeChance = Math.random() + 1
  return (fighter.defense || 0) * dodgeChance;
}

export function getCritPower(fighter) {
  return fighter.attack * 2;
}

function reduceHealthIndicator(side, newHP, fighterHP) {
  const healthIndicator = document.querySelector(`#${side}-fighter-indicator`);
  const restHealth = (newHP * 100) / fighterHP ;
  const finalHealthIndicator = restHealth <= 0 ? 0 : restHealth;

  healthIndicator.style.width = finalHealthIndicator + '%';

  return finalHealthIndicator;
}

function fillCritIndicator(side, critBarProgress) {
  const cridIndicator =  document.getElementById(`${side}-fighter-crit-indicator`)
  cridIndicator.style.width = critBarProgress + '%';

  return cridIndicator;
}

function critBarProgress (side, callback) {
  let critBarProgress = 0;
  fillCritIndicator(side, critBarProgress);
  const critBarInterval = setInterval(() => {
    critBarProgress += 1;
    fillCritIndicator(side, critBarProgress);

    if (critBarProgress >= 100) {
      clearInterval(critBarInterval);
      callback(true);
    }
  }, 100);
}

function printResultToConsole(fighter, health, damageDone) {
  console.log(`Name: ${fighter.name}, health: ${health}, damageDone: ${damageDone}`)
}
